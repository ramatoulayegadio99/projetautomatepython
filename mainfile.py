import tkinter as tk  

# Function to read the automate from a file
def read_automaton(file_path):
    automate = {}
    with open(file_path, 'r') as file:
        # Read the alphabet
        automate['alphabet'] = set(file.readline().strip()[1:-1].split(','))
        # Read the states
        automate['states'] = set(file.readline().strip()[1:-1].split(','))
        # Read the initial state
        automate['initial_state'] = file.readline().strip()
        # Read the final states
        automate['final_states'] = set(file.readline().strip()[1:-1].split(','))
        # Read the transitions
        automate['transitions'] = {}
        for line in file:
            transition = line.strip()[1:-1].split(',')
            automate['transitions'][(transition[0], transition[1])] = transition[2]
    return automate

# Function to display the automate using Tkinter
def display_automaton(automate):
    root = tk.Tk()
    root.title("Automaton")
    root.geometry("600x400")

    grid_frame = tk.Frame(root)
    grid_frame.grid(row=0, column=0)

    tk.Label(grid_frame, text="States", width=10).grid(row=0, column=0)
    for i, symbol in enumerate(automate['alphabet']):
        tk.Label(grid_frame, text=symbol, width=10).grid(row=0, column=i + 1)

    for i, state in enumerate(automate['states']):
        tk.Label(grid_frame, text=state, width=10).grid(row=i + 1, column=0)
        for j, symbol in enumerate(automate['alphabet']):
            transition = automate['transitions'].get((state, symbol), "")
            var = tk.StringVar(value=str(transition))
            text_widget = tk.Entry(grid_frame, textvariable=var, width=10)
            text_widget.grid(row=i + 1, column=j + 1)

    root.mainloop()




# Read the automate from the file
automate = read_automaton("automaton.txt")

# Display the automate
display_automaton(automate)




