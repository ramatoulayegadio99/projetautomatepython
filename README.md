Reconnaissance de Mots par un Automate

Description :
Ce mini-projet vise à développer un programme permettant de reconnaître des mots à l'aide d'un automate. Le programme est écrit en Python et utilise la bibliothèque Tkinter pour lire un automate à partir d'un fichier texte et l'afficher graphiquement.

Fonctionnalités :
Lecture d'un automate à partir d'un fichier texte.
Affichage graphique de l'automate dans une fenêtre Tkinter.
Visualisation des états, de l'alphabet, de l'état initial, des états finaux et des transitions.
